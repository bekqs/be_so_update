import {saveMobile} from './modules/editMobile';

const [
    headerName,
    headerLocation,
    headerPhone
] = document.querySelectorAll('#header-name, #header-location, #header-phone');

const editButton = document.querySelectorAll('.button--edit:not(#edit-mobile)');
const editButtonMobile = document.getElementById('edit-mobile');
const mobileSave = document.getElementById('mobile-save');
const popupDiv = document.getElementById('popup');
const popupInput = document.getElementById('popup-input');
const popupLabel = document.getElementById('popup-label');
const popupSave = document.getElementById('popup-save');
const buttonCancel = document.querySelectorAll('.button--cancel');
const editForm = document.getElementById('edit-form');
const tabLink = document.querySelectorAll('.nav__item');
const tabDiv = document.getElementsByClassName('tab');
let current;

// Check if browser supports addEventListener
function bindEvent(el, eventName, eventHandler) {
    if (el.addEventListener) {
        el.addEventListener(eventName, eventHandler, false);
    } else if (el.attachEvent) {
        el.attachEvent('on' + eventName, eventHandler);
    }
}

// Display div "popup" next to the edit-button
function popup() {
    // Calculate x/y position for edit-button element
    const pos = this.getBoundingClientRect();
    console.log(pos);

    // Previous element sibling of clicked edit-button
    current = this.previousElementSibling;
    // Clear input each time the function is called
    popupInput.value = '';

    popupDiv.classList.add('active');
    popupDiv.style.top = `${pos.top + window.pageYOffset}px`;
    popupDiv.style.left = `${pos.left + 60}px`;

    if (this.id === 'edit-name') {
        popupLabel.innerHTML = 'Full Name';
        popupInput.type = 'text';
    }

    if (this.id === 'edit-website') {
        popupLabel.innerHTML = 'Website';
        popupInput.type = 'url';
    }

    if (this.id === 'edit-phone') {
        popupLabel.innerHTML = 'Phone Number';
        popupInput.type = 'tel';
    }

    if (this.id === 'edit-location') {
        popupLabel.innerHTML = 'City, State & ZIP';
        popupInput.type = 'text';
    }
}

Array.from(editButton).forEach(element => {
    bindEvent(element, 'click', popup);
});

// Function to change innerHTML of the element we're trying to edit
bindEvent(popupSave, 'click', () => {
    current.innerHTML = popupInput.value;

    if (current.id === 'detail-name') {
        headerName.innerHTML = popupInput.value;
    }

    if (current.id === 'detail-location') {
        headerLocation.innerHTML = popupInput.value;
    }

    if (current.id === 'detail-phone') {
        headerPhone.innerHTML = popupInput.value;
    }

    popupDiv.classList.toggle('active');
});

// Function to close "popup" div
Array.from(buttonCancel).forEach(element => {
    bindEvent(element, 'click', (e) => {
        e.preventDefault();
        editForm.classList.remove('active');
        popupDiv.classList.remove('active');
    });
});

// Edit for mobile devices
bindEvent(editButtonMobile, 'click', (e) => {
    e.preventDefault();
    editForm.classList.toggle('active');
});

// Update and save data on mobile devices
bindEvent(editForm, 'submit', saveMobile);

// Tabs 
Array.from(tabLink).forEach(element => {
    element.addEventListener('click', () => {

        let num;

        document.querySelector('.active').classList.remove('active');
        element.classList.add('active');

        for (let i = 0; i < tabLink.length; i++) {
            if (tabLink[i].classList.contains('active')) {
                num = i;
            }
        }

        for (let i = 0; i < tabDiv.length; i++) {
            if (tabDiv[i].classList.contains('tab--current')) {
                tabDiv[i].classList.remove('tab--current');
            }
        }
        tabDiv[num].classList.add('tab--current');
    })
})

// Popup
const logout = document.getElementById('logout');
const authPopup = document.getElementById('auth-popup');

window.addEventListener('click', (e) => {
    if (e.target === logout) {
        authPopup.classList.toggle('active');
    } else {
        authPopup.classList.remove('active');
    }
})